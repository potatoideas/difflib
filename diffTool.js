module.exports = {
    showDiff: showDiff
}

const fs = require('fs')
const util = require('util')
const readFile = util.promisify(fs.readFile)

function showDiff (firstPath, secondPath, ...otherPaths) {
  let filesContentPromises = [readFileIntoArray(firstPath), readFileIntoArray(secondPath)]
  otherPaths.forEach(path => filesContentPromises.push(readFileIntoArray(path)))
  
  Promise
    .all(filesContentPromises)
    .then(fileContents => {
      fileContents.forEach((contents, index) => {
        if (index < fileContents.length - 1) 
          compareContent(Array.from(contents), Array.from(fileContents[index + 1]))
      })
    });
};

function readFileIntoArray (filePath) {
  return readFile(filePath)
    .then(data => {
      return data.toString().split('\n')
    })
    .catch(err => {
      console.log(err)
    })
};

function compareContent(firstContent, secondContent) {
  let lcsMatrix = longestCommonSubsequenceMatrix(firstContent, secondContent),
      lcs = findLongestCommonSubsequence(firstContent, secondContent, lcsMatrix),
      diff = prettifyDiff(firstContent, secondContent, lcs)

  console.log(diff)
};

function longestCommonSubsequenceMatrix (firstContent, secondContent) {
  let matrix = [],
      maxValue = null

  matrix.push(Array(secondContent.length + 1).fill(0))

  firstContent.forEach((firstContentWord, firstContentIndex) => {
    matrix.push([0])
    secondContent.forEach((secondContentWord, secondContentIndex) => {
      if(firstContentWord == secondContentWord) {
        matrix[firstContentIndex + 1].push(matrix[firstContentIndex][secondContentIndex] + 1)
      } else {
        maxValue = Math.max(matrix[firstContentIndex + 1][secondContentIndex], matrix[firstContentIndex][secondContentIndex + 1])
        matrix[firstContentIndex + 1].push(maxValue)
      }
    })
  })

  return matrix
}

function findLongestCommonSubsequence (firstContent, secondContent, matrix) {
  let lcs = [],
      currentValueRow = matrix.length - 1,
      currentValueCol = matrix[0].length - 1,
      currentValue = matrix[currentValueRow][currentValueCol],
      maxValue = null

  while(currentValue != 0) {
    maxValue = Math.max(matrix[currentValueRow][currentValueCol - 1], matrix[currentValueRow - 1][currentValueCol])
    if(currentValue == maxValue) {
      if(maxValue == matrix[currentValueRow][currentValueCol - 1]) {
        currentValueCol = currentValueCol - 1
      } else {
        currentValueRow = currentValueRow - 1
      }
    } else {
      lcs.unshift(firstContent[currentValueRow - 1])
      currentValueRow = currentValueRow - 1
      currentValueCol = currentValueCol - 1
      currentValue = matrix[currentValueRow][currentValueCol]
    }
  }

  return lcs
}

function prettifyDiff (firstContent, secondContent, subsequence) {
  let result = '', 
      lastIndex = 0

  subsequenceWord = subsequence.shift()
  secondContentWord = secondContent.shift()

  firstContent.forEach((firstContentWord, index) => {
    if(subsequenceWord) {
      if(firstContentWord == subsequenceWord && secondContentWord == subsequenceWord) {
        result += lineRemains(index + 1, subsequenceWord)
        subsequenceWord = subsequence.shift()
        secondContentWord = secondContent.shift()
      } else if(firstContentWord != subsequenceWord && secondContentWord == subsequenceWord) {
        result += lineRemoved(index + 1, firstContentWord)
      } else if(firstContentWord == subsequenceWord && secondContentWord != subsequenceWord) {
        result += lineAdded(index + 1, secondContentWord)
        secondContentWord = secondContent.shift()
      } else {
        result += lineModified(index + 1, firstContentWord, secondContentWord)
        secondContentWord = secondContent.shift()
      }
    } else {
      if(secondContentWord) {
        result += lineModified(index + 1, firstContentWord, secondContentWord)
        secondContentWord = secondContent.shift()
      } else {
        result += lineRemoved(index + 1, firstContentWord)
      }
    }
    lastIndex = index + 1
  })

  secondContentWord && secondContent.unshift(secondContentWord)
  secondContent.forEach(secondContentWord => {
    lastIndex++
    result += lineAdded(lastIndex, secondContentWord)
  })
  return result
}

function lineRemains (index, word) {
  return `${index}\t\t${word}\n`
}
function lineRemoved (index, word) {
  return `${index}\t-\t${word}\n`
}
function lineAdded (index, word) {
  return `${index}\t+\t${word}\n`
}
function lineModified (index, firstWord, secondWord) {
  return `${index}\t*\t${firstWord}|${secondWord}\n`
}